pipeline {
    agent any

    parameters {
        //tests to run
        //API
        string(name: 'apiTests', defaultValue: 'ApitestsIT', description: 'API tests')
        //cucumber
        string(name: 'cuke', defaultValue: 'RunCukesIT', description: 'cucumber tests')

        //general
        string(name: 'context', defaultValue: 'safebear', description: 'application context')
        string(name: 'domain', defaultValue: 'http://54.149.156.88', description: 'application context')

        //test environment
        string(name: 'test_hostname', defaultValue: '54.149.156.88', description: 'hostname of the test environment')
        string(name: 'test_port', defaultValue: '8888', description: 'port of the test environment')
        string(name: 'test_username', defaultValue: 'tomcat', description: 'username of the test environment')
        string(name: 'test_password', defaultValue: 'tomcat', description: 'password of the test environment')
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '3', artifactNumToKeepStr: '3'))
    }

    triggers { pollSCM('H/5 * * * *') } // Poll every 5 mins

    //Pipeline stages
    stages {
        stage('Build with Unit Testing') {
            /*run the mvn package command to ensure the build the app and run the unit */

            steps {
                sh 'mvn clean package sonar:sonar -Dsonar.organization=safebear-student-bitbucket -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=f08a36f3598527bf9b20d9b861b57c0aaa5937e3'
            }


            post {
                /* only run if the last step succeeds */
                success {
                    // print a message to screen that we are archiving
                    echo 'Now Archiving...'
                    //archive the artifacts so we can build once and use them later to deploy
                    archiveArtifacts artifacts: '**/target/*.war'
                }

                always {
                    junit "**/target/surefire-reports/*.xml"
                }
            }
        }
        stage('Static Analysis') {
            steps {
                sh 'mvn checkstyle:checkstyle'
            }
            post {
                success {
                    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '', unHealthy: ''
                }
            }
        }
        stage('Deploy to Test') {
            steps {
                sh "mvn cargo:redeploy -Dcargo.hostname=${params.test_hostname} -Dcargo.servlet.port=${params.test_port} -Dcargo.username=${params.test_username} -Dcargo.password=${params.test_password}"
            }
        }
        stage('integration tests') {
            steps {
                sh "mvn -Dtest=${params.apiTests} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"

            }
            post {
                always {
                    junit "**/target/surefire-reports/*ApitestsIT.xml"
                }
            }

        }
        stage('cucumber bdd tests'){

            steps{

                sh "mvn -Dtest=${params.cuke} test -Ddomain=${params.domain} -Dport=${params.test_port} -Dcontext=${params.context}"

            }
            post {
                always{
                    publishHTML([
                            allowMissing            : false,
                            alwaysLinkToLastBuild   : false,
                            keepAll                 : false,
                            reportDir               : 'target/cucumber',
                            reportFiles             : 'index.html',
                            reportName              : 'BDD report',
                            reportTitles            : ''
                    ])
                }
            }

        }
    }
}
//HTML workaround






