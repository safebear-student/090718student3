package com.safebear.tasklist.model;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;

/**
 * Created by cca_student on 10/07/2018.
 */
public class TaskTest {
    @Test
    public void creation() {

        LocalDate localdate = LocalDate.now();

        Task task = new Task(1L, "Configure Jenkins server", localdate, false);
        Assertions.assertThat(task.getId()).isEqualTo(1L);
//      Now lets check the task name isn't blank
        Assertions.assertThat(task.getName()).isNotBlank();
//      And check the task name
        Assertions.assertThat(task.getName()).isEqualTo("Configure Jenkins server");
//      Check date
        Assertions.assertThat(task.getDueDate()).isEqualTo(localdate);
//      Check it's not yet completed
        Assertions.assertThat(task.getCompleted()).isEqualTo(false);



    }
}