package com.safebear.tasklist.usertests;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by cca_student on 09/07/2018.
 */
public class StepDefs {

    @Given("^the following tasks are created:$")
    public void the_following_tasks_are_created(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @When("^the homepage opens$")
    public void the_homepage_opens() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the following tasks appear in the list:$")
    public void the_following_tasks_appear_in_the_list(DataTable arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
        // E,K,V must be a scalar (String, Integer, Date, enum etc)
        throw new PendingException();
    }

    @Given("^a (.+) is in a (.+)$")
    public void a_task_is_in_a_state(String taskname, String status) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a (.+) is updated to a (.+)$")
    public void a_Configure_Jenkins_is_updated_to_a_completed(String taskname, String status) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the (.+) is now in the (.+)$")
    public void the_Configure_Jenkins_is_now_in_the_completed(String taskname, String endstate) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^a user creates a (.+)$")
    public void a_user_creates_a_task(String taskname) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the (.+) appears in the list$")
    public void the_task_appears_in_the_list() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }




}
