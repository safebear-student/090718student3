Feature: Task Management

  User Story:
  In order to mange my tasks
  As a user
  I must be able to view, add, archive, or update tasks.

  Rules:
   - You must be able to view all tasks
   - You must be able to see the name, duedate and status of a task
   - You must be able to change the status of a task
   - You must be ale to archive tasks

  Questions:
  - How many tasks per page for the pagination?
  - Where do you archive to
  - is the due date configurable or should it be hard-coded to a certain number of days in the future

  To do:
  - Pagination

  Domain Language:
  - Task = this is made up of a description, a status and a due date.
  - Due Date = this must be in the format dd/mm/yyyy
  - Status = this is 'completed' or 'not completed'
  - Task List = this is a list of tasks and can be displayed on the home page.

  Background:
    Given the following tasks are created:
    |Configure Jenkins|
    |Set up automation environment|
    |Set up SQL server            |

    Scenario: A user opens the home page
      When the homepage opens
      Then the following tasks appear in the list:
       |Configure Jenkins|
       |Set up automation environment|
       |Set up SQL server            |

      Scenario Outline: A user changes the state of a task
        Given a <task> is in a <startingstate>
        When a <task> is updated to a <endstate>
        Then the <task> is now in the <endstate>
        Examples:
        | startingstate | task | endstate |
        | not completed | Configure Jenkins|completed|


  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
    |task|
    |cooking|
    |gardening|